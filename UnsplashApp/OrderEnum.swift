//
//  OrderEnum.swift
//  UnsplashApp
//
//  Created by Pavel Moroz on 25.09.2020.
//  Copyright © 2020 Mykhailo Romanovskyi. All rights reserved.
//

import Foundation


enum OrderKind: String {

    case latest, oldest, popular
}

enum Order: String {

    case latest, oldest, popular
}
